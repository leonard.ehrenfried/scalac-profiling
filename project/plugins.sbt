libraryDependencies += "com.thesamet.scalapb" %% "compilerplugin" % "0.9.4"

addSbtPlugin("com.thesamet" % "sbt-protoc" % "0.99.23")
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.10")
addSbtPlugin("org.xerial.sbt" % "sbt-sonatype" % "3.8.1")
addSbtPlugin("com.jsuereth" % "sbt-pgp" % "2.0.0")
addSbtPlugin("com.dwijnand" % "sbt-dynver" % "4.0.0")
