/*                                                                                                *\
**      _____            __         ______           __                                           **
**     / ___/_________ _/ /___ _   / ____/__  ____  / /____  _____                                **
**     \__ \/ ___/ __ `/ / __ `/  / /   / _ \/ __ \/ __/ _ \/ ___/    Scala Center                **
**    ___/ / /__/ /_/ / / /_/ /  / /___/ /__/ / / / /_/ /__/ /        https://scala.epfl.ch       **
**   /____/\___/\__,_/_/\__,_/   \____/\___/_/ /_/\__/\___/_/         (c) 2017-2018, LAMP/EPFL    **
**                                                                                                **
\*                                                                                                */

import sbt.Keys.publishArtifact
import sbt.ThisBuild
import xerial.sbt.Sonatype._

// global settings
ThisBuild / scalaVersion := "2.13.1"
ThisBuild / organization := "io.leonard"
ThisBuild / sonatypeProfileName := "io.leonard"
ThisBuild / publishTo := sonatypePublishToBundle.value
ThisBuild / publishMavenStyle := true

// publish settings for the root project, which is not published
publish := {}
publishArtifact := false

import sbt.Keys.scalaOrganization
import scalapb.compiler.Version.scalapbVersion

lazy val commonSettings = Seq(
  sonatypeProjectHosting := Some(GitLabHosting("leonard.ehrenfried", "scalac-profiling", "mail@leonard.io")),
  licenses := Seq("Apache2" -> url("http://www.apache.org/licenses/LICENSE-2.0.txt"))
)

lazy val profiledb = (project in file("profiledb"))
  .settings(
    commonSettings,
    name := "profiledb",
    libraryDependencies ++= Seq(
      "org.scala-lang" % "scala-compiler" % "2.13.1",
      "com.thesamet.scalapb" %% "scalapb-runtime" % scalapbVersion % "protobuf",
      "com.google.protobuf" % "protobuf-java" % "3.11.0"
    ),
    PB.targets in Compile := Seq(
      scalapb.gen() -> (sourceManaged in Compile).value
    )
  )

lazy val plugin = (project in file("plugin"))
  .settings(
    commonSettings,
    name := "scalac-profiling",
    libraryDependencies ++= Seq(
      "com.lihaoyi" %% "pprint" % "0.5.6",
      scalaOrganization.value % "scala-compiler" % scalaVersion.value
    ),
    test in assembly := {},
    packageBin in Compile := (assembly in (Compile)).value,
      assemblyOption in assembly :=
    (assemblyOption in assembly).value
      .copy(includeScala = false, includeDependency = true),
    publishConfiguration := publishConfiguration.value.withOverwrite(true),
    publishLocalConfiguration := publishLocalConfiguration.value.withOverwrite(true),
  ).dependsOn(profiledb)

